import { motion } from "framer-motion";
import styled from "styled-components";

export const StyledAbout = styled(motion.div)`
   height: 90vh;
   padding: 5rem 10rem;
   color: white;
   display: flex;
   align-items: center;
   justify-content: space-between;


   @media (max-width: 768px) {
      display: block;
      padding: 2rem 2rem;
      text-align: center;

      h2 span {
         font-size: 30px;
      }
   }
`;


export const StyledDescription = styled.div`
   flex: 1;
   padding-right: 5rem;
   z-index: 2;
   h2 {
      font-weight: lighter;
   }

   @media (max-width: 768px) {
      padding: 0;

      button {
         margin: 2rem 0rem 5rem 0rem;
      }
   }
`;

export const StyledImg = styled.div`
   border-radius: 10px;
   flex: 1;
   overflow: hidden;

   z-index: 2;
   img {
      width: 100%;
      height: 70vh;
      object-fit: cover;
   }
`;

export const StyledHide = styled.div`
  overflow: hidden;
`;