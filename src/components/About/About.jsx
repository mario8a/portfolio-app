import React from "react";
import { motion } from 'framer-motion';
import photo1 from '../../img/me.jpeg'
import { StyledAbout, StyledDescription, StyledHide, StyledImg } from './AboutStyle';
import { fadeAnimation, photoAnimation, titleAnimation } from '../../animation';
import Wave from './../Wave/Wave';
import { Link } from "react-router-dom";

const About = () => {
   return (
      <StyledAbout>
         <StyledDescription>
            <motion.div>
               <StyledHide>
                  <motion.h2 variants={titleAnimation}>Mario Ochoa</motion.h2>
               </StyledHide>
               <StyledHide>
                  <motion.h2 variants={titleAnimation}>
                     <span>Front End Developer</span>
                  </motion.h2>
               </StyledHide>
            </motion.div>
            <motion.p variants={fadeAnimation}>
               Ingeniero en Computacion FES Aragón UNAM | Apasionado por las tecnologias web, Hackatones, Startup Weekend y los 💚chilaquiles🧡
            </motion.p>
            <Link  to={{ pathname: "https://drive.google.com/file/d/13fHCqHapoC5lzPGIxrh_0LOLHHLrVJD3/view?usp=sharing" }} target="_blank">
               <motion.button>Descargar CV</motion.button>
            </Link>
         </StyledDescription>
         <StyledImg>
            <motion.img variants={photoAnimation} src={photo1} alt="Selfie" />
         </StyledImg>
         <Wave />
      </StyledAbout>
   );
};

export default About;
