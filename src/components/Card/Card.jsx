import React from 'react';

export const Card = ({image, name, demo, repo}) => {
   return (
      <div className="card">
         <img className="card__img" src={image} alt="Earth" />
         <div className="card-content card-content--smSecreen">
            <h2 className="card__title">
               <b> {name} </b>
            </h2>
            <div className="action">
               <a href={demo} title="preview page" target='__blank'>
                  <i className="fas fa-eye"></i>
               </a>
               <a href={repo} title="code" target='__blank'>
                  <i className="fas fa-code"></i>
               </a>
            </div>
         </div>

         <div className="card-content card-content--bgSecreen">
            <h2 className="card__title">
               <b>{name}</b>
            </h2>
            <div className="action">
               <a
                  className="btn code--outline"
                  href={demo}
                  target='__blank'
               >
                  <i className="fas fa-eye"></i>
                  Demo
               </a>
               <a
                  className="btn code--outline"
                  href={repo}
                  target='__blank'
               >
                  <i className="fas fa-code"></i>
                  Repositorio
               </a>
            </div>
         </div>
      </div>
   )
}
