import React from 'react';

//assets
import fullStack from '../../img/full-stack-javascript-santander-universidades.png';
import scrumDev from '../../img/ScrumDeveloper.jpg'
import { AboutMeSetion, Achievments, ContainerSection } from './InfoSectionStyles';

const InfoSection = () => {

   return (
      <AboutMeSetion>
         <ContainerSection>
            <h2> Hola!😀 </h2>
            <p> Soy Mario Ochoa, una persona apasionada por participar en eventos de tecnologia, emprendimiento y el desarrollo de software principalmente en el desarrollo front. Actualmente me desempeño como front-end developer en BBVA. Tambien soy miembro de la comunidad de Startup Weekend CDMX en donde cada año organizamos eventos de emprendimiento de diferentes tematicas </p>
            <p> El stack de tecnologias que manejo son React 💙 y Angular 🧡 </p>
            <p> Cabe mencionar no estoy cerrado a ninguna tecnologia por lo que me gusta aprender cosas nuevas que aporten valor a mi vida profesional :D </p>
         </ContainerSection>
            <div>
               <Achievments>
                  <img src={fullStack} alt="" />
                  <img src={scrumDev} alt="" />
               </Achievments>
            </div>
      </AboutMeSetion>
   )
}

export default InfoSection;