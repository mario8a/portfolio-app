import styled from "styled-components";

/**
 * Si se quiere usar un styled-component reutilizable, pero no queres que se llame igual que como ya existe
 * lo que se hace es usar styled(el stilo que queres usar pero con el nombre original)`` (NO OLVIDAR LOS ``) y listo,
 * lo copado es que se pueden agregar estilos, similar a una herencia
 */

/**
 * Animacion del scroll:
 * Necesito agregarle animacion al styledServices, que sea un motion.div
 * pero el styledAbout NO estaba como motion.div, asi q con ir a editarlo ya se soluciona
 */

 export const Achievments = styled.div`
 width: 100%;
 text-align: center;
 padding: 8rem 0;
 img {
    width: 350px;
 }
 
 @media (min-width: 768px) {
    display: flex;
    justify-content: space-evenly;
    align-items: center;
 }
`;

export const AboutMeSetion = styled.div`
 color: white;
 padding-top: 4rem;
 max-width: 1200px;
 width: 95%;
 height: 70vh;
 margin: 22rem auto;

 @media (min-width: 768px){
    margin: 0rem auto;
    padding: 4rem;
 }
`;

export const ContainerSection = styled.div`
 padding: 30px 20px 0px;
 p {
    padding: 10px;
 }
`