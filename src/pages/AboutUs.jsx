import React from 'react'
import { motion } from 'framer-motion';
// import styled from 'styled-components';
import { pageAnimation } from './../animation';
import About from './../components/About/About';
import InfoSection from './../components/InfoSection/InfoSection';

// const AboutUsSection = styled.div`
//    background: #1b1b1b;
// `;

const AboutUs = () => {
   
   return (
      <motion.div variants={pageAnimation} initial="hidden" animate="show">
         <About />
         <InfoSection />
      </motion.div>
   )
}

export default AboutUs;


//<AboutUsSection> // Esta seccion puede solo quedar color negro
// <AboutSection />
//</AboutUsSection>