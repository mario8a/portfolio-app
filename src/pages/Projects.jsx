import React from 'react'
import { Card } from './../components/Card/Card';

import petgram from '../img/Petgram.jpg';
import movieReact from '../img/react-movie.jpg';
import rick from '../img/react-hooks-api.jpg';
import avocado from '../img/AvocadoApp.jpg';
import iphone from '../img/iphonePage.jpg';
import formAnimate from '../img/formAnimate.jpg';
import textRev from '../img/textReveal.jpg';
import pageTrans from '../img/pageTransition.jpg';

import '../styles/Projects.css';

const projectos = [
   {
      id: 1,
      name : 'Movie App',
      src: movieReact,
      demo: 'https://naughty-tesla-05bc7a.netlify.app/',
      repo: 'https://gitlab.com/mario8a/movieapp-react'
   },
   {
      id: 2,
      name : 'Petgram',
      src: petgram,
      demo: 'https://petgram-mario8a.vercel.app/',
      repo: 'https://gitlab.com/mario8a/petgram'
   },
   {
      id: 3,
      name : 'Rick & Morty API',
      src: rick,
      demo: 'https://priceless-bhabha-4b3238.netlify.app/',
      repo: 'https://gitlab.com/mario8a/react-hooks'
   },
   {
      id: 4,
      name : 'Avocado App',
      src: avocado,
      demo: 'https://avocado-next-lp0niceh5-mario8a.vercel.app/',
      repo: 'https://gitlab.com/mario8a/avocado-next'
   },
   {
      id: 5,
      name : 'Form animation',
      src: formAnimate,
      demo: 'https://forme-animate.netlify.app/',
      repo: 'https://github.com/mario8a/js-creattive'
   },
   {
      id: 6,
      name : 'Page transitions',
      src: pageTrans,
      demo: 'https://animatetransition.netlify.app/',
      repo: 'https://github.com/mario8a/js-creattive'
   },
   {
      id: 7,
      name : 'Text reveal',
      src: textRev,
      demo: 'https://text-revealanimate.netlify.app/',
      repo: 'https://github.com/mario8a/js-creattive'
   },
   {
      id: 6,
      name : 'Iphone page',
      src: iphone,
      demo: 'https://festive-goodall-b74c53.netlify.app/',
      repo: 'https://github.com/mario8a/js-creattive'
   }
]

const Projects = () => {
   return (
      <section className="container-projects">
         <div className="title-section">
            <h2>My projects</h2>
         </div>
         <div className="cards">
            {
               projectos.map(project => (
                  <Card 
                     key={project.id}
                     image={project.src}
                     name={project.name}
                     demo={project.demo}
                     repo={project.repo}
                  />
               ))
            }
         </div>
      </section>  
   )
}

export default Projects
